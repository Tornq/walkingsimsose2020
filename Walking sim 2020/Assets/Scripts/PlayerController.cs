﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float speed = 4f;
    [SerializeField]
    private string sidewaysAxis = "Horizontal";
    [SerializeField]
    private string forwardAxis = "Vertical";
    [SerializeField]
    private CharacterController character;
    [SerializeField]
    private float rotationSpeed = 180f;
    [SerializeField]
    private string yRotationAxis = "Mouse X";
    [SerializeField]
    private string xRotationAxis = "Mouse Y";
    
    [SerializeField]
    private float minRotX = -70;
    [SerializeField]
    private float maxRotX = 80;

    [SerializeField]
    private new Transform camera;
    
    private float currentRotX = 0f;
    
    [SerializeField]
    private LayerMask detectionMask;
    
    [SerializeField]
    private string interactButton = "Interact";
    
    [SerializeField]
    private string jumpButton = "Jump";
    
    private float yVelocity;
    [SerializeField]
    private float gravityMultiplier = 1;
    [SerializeField]
    private float jumpSpeed = 3;

    private Vector3 lastGroundHit;
    
    private void Start()
    {
        if (character == null) 
        {
            character = GetComponent<CharacterController>();
        }
        if (camera == null) 
        {
            if (transform.childCount == 0) 
            {
                GameObject cam = new GameObject("Camera");
                cam.transform.parent = this.transform;
                cam.transform.localPosition = Vector3.zero;
                cam.AddComponent<Camera>();
                cam.AddComponent<AudioListener>();

                camera = cam.transform; 
                return; 
            }
            else 
            {
                camera = transform.GetChild(0);
            }
        }
    }
    
    void Update()
    {
        Rotation();
        Move();
    }

    void Move()
    {
        float xMovement = Input.GetAxis(axisName: sidewaysAxis);
        float zMovement = Input.GetAxis(axisName: forwardAxis);

        
        Vector3 movement = transform.forward * zMovement + transform.right * xMovement;
        
        movement.Normalize(); 
        movement *= speed * Time.deltaTime;

        
        if (Physics.SphereCast(transform.position, 0.5f, Vector3.down, out RaycastHit groundHit, 0.6f))
        {
            lastGroundHit = groundHit.point;
            if (Input.GetButtonDown(buttonName: jumpButton))
            {
                yVelocity = jumpSpeed;
            }
        }
        else
        {
            yVelocity += Physics.gravity.y * Time.deltaTime * gravityMultiplier;
        }
        movement.y += yVelocity * Time.deltaTime;
        
        character.Move(movement);
    }
    
    void Rotation()
    {
        float yRotation = Input.GetAxis(axisName: yRotationAxis);
        yRotation *= rotationSpeed * Time.deltaTime;
        
        float xRotation = Input.GetAxis(axisName: xRotationAxis);
        xRotation *= rotationSpeed * Time.deltaTime;
        
        currentRotX = currentRotX + xRotation;
        currentRotX = Mathf.Clamp(currentRotX, minRotX, maxRotX);
        
        transform.Rotate(0, yRotation, 0);
        camera.localEulerAngles = new Vector3(currentRotX, 0, 0);
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(lastGroundHit, 0.5f);
    }
}